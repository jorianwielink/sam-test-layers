require 'plate_api_main_helper'
require 'json'

def lambda_handler(event:, context:)

  msg = test_me

  {
    statusCode: 200,
    body: {
      message: msg,
    }.to_json
  }
end
